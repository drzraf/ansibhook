# -*- coding: utf-8 -*-
# Copyright (c) 2022, Raphaël . Droz + floss @ gmail DOT com
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type

# Python code that runs on the remote machine as part of the AnsiBallz
# Both this file (and base_basic.py we override) are bundled, in the same directory.

from ansible.module_utils import base_basic
from ansible.module_utils.base_basic import AnsibleModule as OrigAnsibleModule

import sys, os, json, re
from ansible.module_utils.six import (PY2, PY3)
from ansible.module_utils.common.text.converters import (container_to_bytes as json_dict_unicode_to_bytes)

_ANSIBLE_ARGS = base_basic._ANSIBLE_ARGS
PASS_VARS = base_basic.PASS_VARS

PASS_VARS['become'] = ('become', None)

# Needs to be override (unmodified) simply to maintain _ANSIBLE_ARGS context
# Not doing so would lead to null _ANSIBLE_ARGS when called on the remote host
# leading to python waiting upon stdin.
def _load_params():
    ''' read the modules parameters and store them globally.

    This function may be needed for certain very dynamic custom modules which
    want to process the parameters that are being handed the module.  Since
    this is so closely tied to the implementation of modules we cannot
    guarantee API stability for it (it may change between versions) however we
    will try not to break it gratuitously.  It is certainly more future-proof
    to call this function and consume its outputs than to implement the logic
    inside it as a copy in your own code.
    '''
    global _ANSIBLE_ARGS
    if _ANSIBLE_ARGS is not None:
        buffer = _ANSIBLE_ARGS
    else:
        # debug overrides to read args from file or cmdline

        # Avoid tracebacks when locale is non-utf8
        # We control the args and we pass them as utf8
        # print(sys.argv)
        if len(sys.argv) > 1:
            if os.path.isfile(sys.argv[1]):
                fd = open(sys.argv[1], 'rb')
                buffer = fd.read()
                fd.close()
            else:
                buffer = sys.argv[1]
                if PY3:
                    buffer = buffer.encode('utf-8', errors='surrogateescape')
        # default case, read from stdin
        else:
            if PY2:
                buffer = sys.stdin.read()
            else:
                buffer = sys.stdin.buffer.read()
        _ANSIBLE_ARGS = buffer

    try:
        params = json.loads(buffer.decode('utf-8'))
    except ValueError:
        # This helper used too early for fail_json to work.
        print('\n{"msg": "Error: Module unable to decode valid JSON on stdin.  Unable to figure out what parameters were passed", "failed": true}')
        sys.exit(1)

    if PY2:
        params = json_dict_unicode_to_bytes(params)

    try:
        return params['ANSIBLE_MODULE_ARGS']
    except KeyError:
        # This helper does not have access to fail_json so we have to print
        # json output on our own.
        print('\n{"msg": "Error: Module unable to locate ANSIBLE_MODULE_ARGS in json data from stdin.  Unable to figure out what parameters were passed", '
              '"failed": true}')
        sys.exit(1)


base_basic._load_params = _load_params


# Thanks to our "normal" ActionModule override which sets modulesargs['_ansible_become'] = kwargs
# and to above PASS_VARS, our (remote) module execution now has access a `self.become` property.
class AnsibleModule(OrigAnsibleModule):

    def __init__(self, argument_spec, bypass_checks=False, no_log=False,
                 mutually_exclusive=None, required_together=None,
                 required_one_of=None, add_file_common_args=False,
                 supports_check_mode=False, required_if=None, required_by=None):
        super(AnsibleModule, self).__init__(argument_spec, bypass_checks, no_log,
                 mutually_exclusive, required_together,
                 required_one_of, add_file_common_args,
                 supports_check_mode, required_if, required_by)

    # See directsudo.py for the reference.
    def _remote_build_become_command(self, cmd):
        becomecmd = self.become['become_exe'] or 'sudo'
        flags = self.become['become_flags'] or ''
        user = self.become['become_user'] or ''
        if user:
            user = '-u %s' % (user)
        return ' '.join([becomecmd, flags, user, cmd])

    def run_command(self, args, **kwargs):
        orig_args = args
        if self._name in ['composer', 'git', 'unarchive'] and self.become:
            # args can be either a <list> or a <str>
            cmd_args = args if isinstance(args, str) else ' '.join(args)
            if self._name == 'composer' and re.search(r'composer +help', cmd_args):
                pass
            elif self._name == 'git' and re.search(r'git (--version|ls-remote)', cmd_args):
                pass
            else:
                # https://github.com/ansible-collections/community.general/issues/5204
                # set --working-dir= first
                if self._name == 'composer':
                    self.warn("Workaround composer argument ordering")
                    cmd_args = re.sub(r"/composer (.*) (--working-dir '.*?').*", r'/composer \2 \1', cmd_args, 1)

                args = self._remote_build_become_command(cmd_args)

        self.warn("[%s] executes \"%s\" [passed as %s]" % (self._name, args, type(orig_args)))
        return super(AnsibleModule, self).run_command(args, **kwargs)
