<?php

# Copyright (c) 2022, Raphaël . Droz + floss @ gmail DOT com
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

use Gelf\Publisher;
use Gelf\Transport\UdpTransport;
use Gelf\Logger;

define('GELF_PHAR', '/usr/local/bin/gelf-php.phar');
define('ANSIBLE_BIN', getenv('ANSIBLE_BIN') ?: '/usr/bin/ansible-playbook');
define('DEPLOY_AS', getenv('DEPLOY_AS') ?: null);
define('LOGFILE', PHP_SAPI === 'cli' ? 'php://stderr' : '/var/log/gitlab-deploy.log');
date_default_timezone_set('UTC');

$logfile = null;
$logger = null;

function init_log()
{
    global $logfile, $logger;
    $logfile = @fopen(LOGFILE, 'a');
    if (! $logfile) {
        $logfile = fopen('php://stderr', 'a');
    }

    if (!empty(getenv('GRAY_LOG_TOKEN'))) {
        //@include_once('phar://' . GELF_PHAR . '/vendor/autoload.php');
        if (class_exists('Gelf\Logger')) {
            $transport = new UdpTransport(getenv('GRAY_LOG_HOST'), getenv('GRAY_LOG_PORT') ?: 2202);
            // $transport->setMessageEncoder(new Gelf\Encoder\JsonEncoder());
            $publisher = new Publisher();
            $publisher->addTransport($transport);
            $logger = new Logger($publisher, 'gitlab-webhook', ['X-OVH-TOKEN' => getenv('GRAY_LOG_TOKEN')]);
        }
    }
}

function interpolate($str, $json)
{
    $payload = $json;
    return eval('return "' . $str . '";');
}

function ok()
{
    ob_start();
    header("HTTP/1.1 200 OK");
    header("Connection: close");
    header("Content-Length: " . ob_get_length());
    ob_end_flush();
    @ob_flush();
    flush();
}

function error()
{
    ob_start();
    header("HTTP/1.1 500 Internal Server Error");
    header("Connection: close");
    header("Content-Length: " . ob_get_length());
    ob_end_flush();
    ob_flush();
    flush();
}

function quit()
{
    global $logfile;
    fclose($logfile);
    exit;
}

// log (prepending time)
function logit($str)
{
    global $logfile, $logger;
    fputs($logfile, date("Y-m-d H:i:s") . ': ' . $str);
    if ($logger) {
        $logger->alert($str);
    }
}

function should_ignore_server($hostname, $msg)
{
    if (! trim($msg)) {
        return false;
    }

    $msg = str_replace("\n", '\n', trim($msg));
    $matches = [];
    if (!preg_match('/\bdeploy:(?:db|server|env):(?P<servers>.+)/', $msg, $matches)) {
        return false;
    }

    logit(sprintf('=== commit msg "%s" may limit deployments to specific hosts ===' . PHP_EOL, $msg));
    $listing = explode(',', $matches['servers']);
    $defaultIsSkip = (count($listing) > 0 && strpos($listing[0], '+') === 0);
    if (in_array('-' . $hostname, $listing) || in_array('!' . $hostname, $listing)) {
        // logit("Skip deployment for $hostname because of the commit-message" . PHP_EOL);
        return true;
    }
    if (in_array('+' . $hostname, $listing) || in_array($hostname, $listing)) {
        return false;
    }

    if ($defaultIsSkip) {
        // logit("Skip deployment for $hostname (defaults) because of the commit-message" . PHP_EOL);
        return $defaultIsSkip;
    }
}

foreach (
    [
        'GITLAB_TOKEN',
        'COMPOSER_DISCARD_CHANGES',
        'COMPOSER_HOME',
        'COMPOSER_MEMORY_LIMIT',
        'COMPOSER_PROCESS_TIMEOUT',
        'COMPOSER_ALLOW_SUPERUSER'
    ] as $i
) {
    if (!empty(getenv("$i"))) {
        putenv("$i=" . getenv("$i"));
    }
}

$content = file_get_contents(PHP_SAPI === 'cli' ? 'php://stdin' : 'php://input');
$json    = json_decode($content, true);

$kind = $json['object_kind'] ?? '';
$name = $json['project']['name'] ?? '';
init_log();

if (($env = getenv('INVENTORY_HOSTNAME'))) {
    $msg = $json['commit']['message'] ?? '';
    if (! $msg && isset($json['commits']) && ($c = count($json['commits'])) > 0) {
        $msg = $json['commits'][$c - 1]['message'] ?? '';
    }

    if (should_ignore_server($env, $msg)) {
        logit(
            sprintf(
                '=== "%s" on "%s" webhook commit msg meant to skip "%s" ===' . PHP_EOL,
                $kind,
                $name,
                $env
            )
        );
        ok();
        exit;
    }
}

$is_test = isset($_SERVER['HTTP_X_TEST']) || (PHP_SAPI === 'cli' && getenv('TEST'));
$is_vvv = $is_test || isset($_SERVER['HTTP_X_VVV']) || (PHP_SAPI === 'cli' && getenv('VVV'));
$headers = getallheaders() + $_SERVER;
unset($headers['X-Gitlab-Token']);

chdir(__DIR__);
$cmd = sprintf(
    "%s playbook.yml %s %s -l localhost -e remote_tmp=/tmp -e deploy_as=%s -e %s",
    ANSIBLE_BIN,
    $is_test ? '-C' : '',
    $is_vvv ? '-vvvv' : '',
    DEPLOY_AS,
    escapeshellarg(json_encode(['http_headers' => $headers, 'payload' => $json]))
);

$status = $json['object_attributes']['status'] ?? null;
// ToDo: What if no "status" field?
$exit_now = $status && $status !== 'success';


logit(sprintf("PATH=%s" . PHP_EOL, getenv('PATH')));
logit(
    sprintf(
        '=== %s %s = %s. Action = %s ===' . PHP_EOL,
        $name,
        $kind,
        $status ?? '(null)',
        $exit_now ? 'exit' : 'proceed'
    )
);

if ($exit_now) {
    ok();
    quit();
}

// Proceed
try {
    putenv('HOME=/tmp/.auto-ansible-' . posix_getuid());
    putenv('ANSIBLE_REMOTE_TMP=/tmp/');
    putenv('ANSIBLE_NOCOWS=1');
    putenv('ANSIBLE_KEEP_REMOTE_FILES=1');
    putenv('PATH=' . getenv('PATH')); // PATH forwarding is needed
    if (!$is_test && !$is_vvv) {
        putenv('ANSIBLE_DISPLAY_SKIPPED_HOSTS=0');
    }


    $safe_cmd_log = $cmd;
    $private_headers = ['GITLAB_TOKEN', 'HTTP_X_GITLAB_TOKEN', 'GRAY_LOG_TOKEN'];

    foreach ($private_headers as $h) {
        if (!empty($headers[$h])) {
            $safe_cmd_log = preg_replace('/' . preg_quote($headers[$h]) . '/', 'xxx', $safe_cmd_log);
        }
    }

    logit("CMD: " . $safe_cmd_log . PHP_EOL);
    $ret_cmd = shell_exec("{ " . $cmd . "; } 2>&1");
} catch (Exception $e) {
    logit($e . PHP_EOL);
    logit("*** CMD ERROR ***" . PHP_EOL);
} finally {
    logit($ret_cmd . PHP_EOL);
    ok();
    quit();
}
