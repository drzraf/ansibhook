# -*- coding: utf-8 -*-
# Copyright (c) 2022, Raphaël . Droz + floss @ gmail DOT com
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.utils.display import Display
# from ansible.plugins.action import ActionBase
from ansible.plugins.action.command import ActionModule as CommandModule
from ansible.plugins.loader import become_loader

import sys, os
dir_path = os.path.dirname(os.path.realpath(__file__))
if dir_path not in sys.path:
    sys.path.append(dir_path)
from _extension import ExtensionActionModule

display = Display(5)

class ActionModule(CommandModule, ExtensionActionModule):
    """
    This module overrides lib/ansible/plugins/action/command.py which is used for the "command" module
    It behaves a bit differently from "normal.py" which passes `become` information so that `sudo` prefixing
    append on the remote machine.
    This one alter the parameters *before* the AnsiBallZ is created and as such can directly rely upon the proper
    `directsudo` become_method.
    """
    def filter_become_before_modify(self, module_name, module_path, module_args, use_vars, final_environment):
        if self._connection.become:
            become_plugin = become_loader.get('directsudo')
            if become_plugin and '_raw_params' in module_args and module_args['_raw_params']:
                become_kwargs = {}
                display.warning("Detected the use of a \"command\" module using \"become\". Enabling 'directsudo'")

                # Gather contextual become options in order to initialize our directsudo plugin.
                become_plugin.set_options(direct=self._connection.become._options)
                directcmd = become_plugin.build_become_command(module_args['_raw_params'], self._connection._shell)
                display.vvv('[directsudo] command substitution %s' % directcmd)
                module_args['_raw_params'] = directcmd
                self._connection.become = None
                return (module_name, module_path, module_args, use_vars, final_environment, become_kwargs)

        return super(ActionModule, self).filter_become_before_modify(module_name, module_path, module_args, use_vars, final_environment)
