# -*- coding: utf-8 -*-
# Copyright (c) 2022, Raphaël . Droz + floss @ gmail DOT com
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.plugins.action import ActionBase
from ansible.errors import AnsibleError
from ansible.executor.module_common import modify_module
from ansible.executor.interpreter_discovery import discover_interpreter, InterpreterDiscoveryRequiredError

try:
    from ansible.utils.collection_loader import resource_from_fqcr
except:
    # pre-2.11
    from ansible.module_utils.common.text.converters import to_text
    def resource_from_fqcr(ref):
        """
        Return resource from a fully-qualified collection reference,
        or from a simple resource name.
        For fully-qualified collection references, this is equivalent to
        ``AnsibleCollectionRef.from_fqcr(ref).resource``.
        :param ref: collection reference to parse
        :return: the resource as a unicode string
        """
        ref = to_text(ref, errors='strict')
        return ref.split(u'.')[-1]

from ansible.utils.unsafe_proxy import AnsibleUnsafeText

class ExtensionActionModule(ActionBase):

    def _configure_module(self, module_name, module_args, task_vars):
        '''
        Handles the loading and templating of the module code through the
        modify_module() function.
        '''
        if self._task.delegate_to:
            use_vars = task_vars.get('ansible_delegated_vars')[self._task.delegate_to]
        else:
            use_vars = task_vars

        split_module_name = module_name.split('.')
        collection_name = '.'.join(split_module_name[0:2]) if len(split_module_name) > 2 else ''
        leaf_module_name = resource_from_fqcr(module_name)

        # Search module path(s) for named module.
        for mod_type in self._connection.module_implementation_preferences:
            # Check to determine if PowerShell modules are supported, and apply
            # some fixes (hacks) to module name + args.
            if mod_type == '.ps1':
                # FIXME: This should be temporary and moved to an exec subsystem plugin where we can define the mapping
                # for each subsystem.
                win_collection = 'ansible.windows'
                rewrite_collection_names = ['ansible.builtin', 'ansible.legacy', '']
                # async_status, win_stat, win_file, win_copy, and win_ping are not just like their
                # python counterparts but they are compatible enough for our
                # internal usage
                # NB: we only rewrite the module if it's not being called by the user (eg, an action calling something else)
                # and if it's unqualified or FQ to a builtin
                if leaf_module_name in ('stat', 'file', 'copy', 'ping') and \
                        collection_name in rewrite_collection_names and self._task.action != module_name:
                    module_name = '%s.win_%s' % (win_collection, leaf_module_name)
                elif leaf_module_name == 'async_status' and collection_name in rewrite_collection_names:
                    module_name = '%s.%s' % (win_collection, leaf_module_name)

                # TODO: move this tweak down to the modules, not extensible here
                # Remove extra quotes surrounding path parameters before sending to module.
                if leaf_module_name in ['win_stat', 'win_file', 'win_copy', 'slurp'] and module_args and \
                        hasattr(self._connection._shell, '_unquote'):
                    for key in ('src', 'dest', 'path'):
                        if key in module_args:
                            module_args[key] = self._connection._shell._unquote(module_args[key])

            result = self._shared_loader_obj.module_loader.find_plugin_with_context(module_name, mod_type, collection_list=self._task.collections)

            if not result.resolved:
                if result.redirect_list and len(result.redirect_list) > 1:
                    # take the last one in the redirect list, we may have successfully jumped through N other redirects
                    target_module_name = result.redirect_list[-1]

                    raise AnsibleError("The module {0} was redirected to {1}, which could not be loaded.".format(module_name, target_module_name))

            module_path = result.plugin_resolved_path
            if module_path:
                break
        else:  # This is a for-else: http://bit.ly/1ElPkyg
            raise AnsibleError("The module %s was not found in configured module paths" % (module_name))

        # insert shared code and arguments into the module
        final_environment = dict()
        self._compute_environment_string(final_environment)

        ##################### Customize here (see PR https://github.com/ansible/ansible/pull/77503)
        (module_name, module_path, module_args, use_vars, final_environment, become_kwargs) = self.filter_become_before_modify(
            module_name,
            module_path,
            module_args,
            use_vars,
            final_environment)

        # modify_module will exit early if interpreter discovery is required; re-run after if necessary
        for dummy in (1, 2):
            try:
                (module_data, module_style, module_shebang) = modify_module(module_name, module_path, module_args, self._templar,
                                                                            task_vars=use_vars,
                                                                            module_compression=self._play_context.module_compression,
                                                                            async_timeout=self._task.async_val,
                                                                            environment=final_environment,
                                                                            remote_is_local=bool(getattr(self._connection, '_remote_is_local', False)),
                                                                            **become_kwargs)
                break
            except InterpreterDiscoveryRequiredError as idre:
                self._discovered_interpreter = AnsibleUnsafeText(discover_interpreter(
                    action=self,
                    interpreter_name=idre.interpreter_name,
                    discovery_mode=idre.discovery_mode,
                    task_vars=use_vars))

                # update the local task_vars with the discovered interpreter (which might be None);
                # we'll propagate back to the controller in the task result
                discovered_key = 'discovered_interpreter_%s' % idre.interpreter_name

                # update the local vars copy for the retry
                use_vars['ansible_facts'][discovered_key] = self._discovered_interpreter

                # TODO: this condition prevents 'wrong host' from being updated
                # but in future we would want to be able to update 'delegated host facts'
                # irrespective of task settings
                if not self._task.delegate_to or self._task.delegate_facts:
                    # store in local task_vars facts collection for the retry and any other usages in this worker
                    task_vars['ansible_facts'][discovered_key] = self._discovered_interpreter
                    # preserve this so _execute_module can propagate back to controller as a fact
                    self._discovered_interpreter_key = discovered_key
                else:
                    task_vars['ansible_delegated_vars'][self._task.delegate_to]['ansible_facts'][discovered_key] = self._discovered_interpreter

        return (module_style, module_shebang, module_data, module_path)

    # This is the default implementation
    def filter_become_before_modify(self, module_name, module_path, module_args, use_vars, final_environment):
        become_kwargs = {}
        if self._connection.become:
            become_kwargs['become'] = True
            become_kwargs['become_method'] = self._connection.become.name
            become_kwargs['become_user'] = self._connection.become.get_option('become_user',
                                                                              playcontext=self._play_context)
            become_kwargs['become_password'] = self._connection.become.get_option('become_pass',
                                                                                  playcontext=self._play_context)
            become_kwargs['become_flags'] = self._connection.become.get_option('become_flags',
                                                                               playcontext=self._play_context)
        return (module_name, module_path, module_args, use_vars, final_environment, become_kwargs)
