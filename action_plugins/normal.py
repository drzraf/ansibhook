# -*- coding: utf-8 -*-
# Copyright (c) 2022, Raphaël . Droz + floss @ gmail DOT com
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.utils.display import Display
from ansible.plugins.action.normal import ActionModule as NormalModule

import sys, os
dir_path = os.path.dirname(os.path.realpath(__file__))
if dir_path not in sys.path:
    sys.path.append(dir_path)
from _extension import ExtensionActionModule

display = Display(5)

# Run on the controller. Setup the AnsiBallZ for "normal" tasks
class ActionModule(NormalModule, ExtensionActionModule):

    def filter_become_before_modify(self, module_name, module_path, module_args, use_vars, final_environment):
        become_kwargs = {}
        if self._connection.become:
            kwargs = {
                'become': True,
                'become_method': self._connection.become.name,
                'become_user': self._connection.become.get_option('become_user', playcontext=self._play_context),
                'become_password': self._connection.become.get_option('become_pass', playcontext=self._play_context),
                'become_flags': self._connection.become.get_option('become_flags', playcontext=self._play_context)
            }

            # Pass "become" parameters down to the AnsibleModule
            if self._connection.become.name == 'directsudo':
                kwargs['become_exe'] = self._connection.become.get_option('become_exe', playcontext=self._play_context)
                module_args['_ansible_become'] = kwargs
                self._connection.become = None
            else:
                become_kwargs = kwargs

        return (module_name, module_path, module_args, use_vars, final_environment, become_kwargs)
