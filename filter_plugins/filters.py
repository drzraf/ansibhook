# -*- coding: utf-8 -*-
# Copyright (c) 2022, Raphaël . Droz + floss @ gmail DOT com
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

def sudo(user, group):
    return f'sudo -nu { user } -g { group }'

# git pull creates files/dirs, these may be overriden by composer artifacts. Owner of these files must match
def cmd_git_pull(adir):
    return f"/usr/bin/git -C '{ adir }' pull"

#def cmd_git_last_tag(adir):
#    return f"/usr/bin/git -C '{ adir }' describe --tags --abbrev=0"

class FilterModule(object):
    def filters(self):
        """Return the filter list."""
        return {
            'sudo': sudo,
            'cmd_git_pull': cmd_git_pull,
        }
