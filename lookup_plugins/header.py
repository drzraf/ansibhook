# -*- coding: utf-8 -*-
# Copyright (c) 2022, Raphaël . Droz + floss @ gmail DOT com
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.errors import AnsibleOptionsError
from ansible.plugins.lookup import LookupBase

class LookupModule(LookupBase):

  def run(self, terms, variables=None, **kwargs):
    key = terms[0]
    try:
       value = terms[1]
    except:
       pass

    if not isinstance(value, str):
       raise AnsibleOptionsError('|failed expects a <string>')

    try:
       headers = variables['http_headers']
    except KeyError:
       raise AnsibleOptionsError('|failed No http_headers defined')
        
    if value:
       return [key in headers and headers[key] == value]
    return [headers[key] if key in headers else False]
